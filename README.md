# Generate Lab2Fab Workflow

## Setup
#### Install
------
After installing the `generate-workflow` folder, we need to setup and install the program. This program was designed for a UNIX system and therefore should be ran in one.

------

#### Configuration
----
First, we need to run the `setup.sh` shell script by opening a terminal, navigating to the location of the `generate-workflow` folder (usually in `Downloads`).
```shell
cd ~/Downloads
```

Next we need to run the script and pass it your username with the `-u` flag:
```shell
./setup -u myusername
```

This will generate a `Makefile` in the folder. So now, this Makefile needs to be ran with elevated permissions:
```shell
sudo make
```

This will create a binary executable in `/usr/bin` called `generate-workflow`, that you can now call, and pass it a `.xlsx` or `.xls` file. (Excel Spreadsheet)

## Running Program
When you create an excel spreadsheet (ex: `spreadsheet.xlsx`) following the format in the example spreadsheet in the folder (`default-spreadsheet-example.xslx`), place the spreadsheet in this folder, and the program will look in here for your file when you run it.

Examples:
```shell
generate spreadsheet.xlsx
```

This will output a JSON file with the same name as the spreadsheet, in the `data/` folder.

