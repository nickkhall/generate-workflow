import csv
import json
import sys

def process_csv(csv_file, json_output):
    steps = []

    with open(csv_file, newline='', encoding='utf-8') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            step = {"title": row["Steps Title"], "image": row["Steps Main Image"]}

            # Process highlighted images
            h_images = row.get("Steps Highlighted Images", "")
            h_text = row.get("Steps Highlighted Text", "")
            if h_images and h_text:
                h_images_arr = h_images.split("|")
                h_text_arr = h_text.split("|")
                highlighted_images = [{"image": image.strip(), "text": text.strip()} for image, text in zip(h_images_arr, h_text_arr)]
                step["highlightedImages"] = highlighted_images

            # Process text/multilineText
            multiline_text = row.get("Steps Main Text", "")
            if multiline_text:
                multiline_text_arr = multiline_text.split("|")
                if len(multiline_text_arr) == 1:
                    step["text"] = multiline_text_arr[0].strip()
                else:
                    step["multilineText"] = [text.strip() for text in multiline_text_arr]

            # Append non-empty steps
            if any(step.values()):
                steps.append(step)

    output = {"steps": steps}

    with open(json_output, 'w') as jsonfile:
        json.dump(output, jsonfile, indent=2)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 csv_to_json.py <input_csv_file> <output_json_file>")
        sys.exit(1)

    csv_file = sys.argv[1]
    json_output = sys.argv[2]

    process_csv(csv_file, json_output)

