#! /usr/bin/env bash

# global variables
path=''
file=''
xls_filename_without_ext=''
converted_csv_file=''

# script arguments
args=( "$@" )

##################################################
#                     Help                       #
##################################################

for ((i = 0; i < $#; i++)); do
  case "${args[$(($i))]}" in
    -h | --help )
      echo ""
      echo "-----------------------------------------------------------------"
      echo ""
      echo -e $'A bash script generating \e[34mLab2Fab\e[37m workflow config files.'
      echo -e $'i.e \e[33msteps.ts\e[37m'
      echo ""
      echo "-----------------------------------------------------------------"
      echo $'\e[31mWARNING\e[39m: THE WRITE FILE YOU USE WILL BE ERASED TO WRITE NEW DATA.'
      echo "-----------------------------------------------------------------"
      echo -e $'REQUIRED*'
      echo $'\e[33mFile Flags\e[39m:'
      echo ""
      echo -e $'\e[32m-h  \e[39mor \e[32m--help    \e[39mPrint out help.'
      echo "-----------------------------------------------------------------"
      echo -e $'\e[32m-f  \e[39mor \e[32m--file\e[39m*   \e[39mThe filepath to the file to read from to generate the config file.'
      echo "-----------------------------------------------------------------"
      echo -e $'\e[32m-p  \e[39mor \e[32m--path\e[39m    \e[39mThe filepath or the file to write the config file to.'
      echo -e $'                 \e[31mNOTE\e[37m: If you do not specify a path, then it will use the path of the xls file, and if you do specify a path, but do not specify a file name, the file will be named `\e[32msteps.ts\e[37m` by default.'
      echo "-----------------------------------------------------------------"
      echo -e $'\e[32m-csv  \e[39mor \e[32m--csv\e[39m   \e[39mThe filepath to the csv file to read from to generate the config file.'
      echo "-----------------------------------------------------------------"
      echo ""
      echo -e $'\e[35mEXAMPLES\e[39m:'
      echo "./generate-steps.sh -f file.xlsx"
      echo ""
      echo "-----------------------------------------------------------------"
      echo ""
      echo "./generate-steps.sh -f file.csv"
      echo "-----------------------------------------------------------------"
      echo ""
      exit 1;
      ;;
    -f | --file )
      file="${args[$((i+1))]}"
      ;;
    -p | --path )
      path="${args[$((i+1))]}"
      ;;
    -csv )
      csv_file="${args[$((i+1))]}"
      ;;
    *)
      ;;
  esac
done

##################################################
#               Handler Functions                #
##################################################

process_csv_file() {
  local filename=$(get_xls_filename)
  local data_dir="$path/data"
  local json_output="$data_dir/$filename.json"

  # Check if the data directory exists, if not, create it
  if [ ! -d "$data_dir" ]; then
    mkdir -p "$data_dir"
  fi

  python3 csv_to_json.py "$csv_file" "$json_output"
}

handle_xls_file() {
  if convert_xls_to_csv; then
    if [[ -n "$converted_csv_file" ]]; then
      process_csv_file    
    fi
  fi
}

handle_xlsx_file() {
  if convert_xlsx_to_csv; then
    if [[ -n "$converted_csv_file" ]]; then
      process_csv_file    
    fi
  fi
}

check_for_xls2csv() {
  if ! command -v xls2csv &> /dev/null; then
    print_uninstalled_xls_failure_message
    return 1;
  else
    return 0;
  fi 
}

check_for_xlsx2csv() {
  if ! command -v xlsx2csv &> /dev/null; then
    print_uninstalled_xlsx_failure_message
    return 1;
  else
    return 0;
  fi 
}


##################################################
#                  Util Functions                #
##################################################

set_path_to_pwd() {
  path=$(pwd)
}

get_xls_filename() {
  echo "$xls_filename_without_ext"
}

get_xls_file_ext() {
  echo "${file##*.}"
}

set_xls_filename_without_ext() {
  filename="${file##*/}"
  xls_filename_without_ext="${filename%.*}"
}

# converts .xlsx file to .csv
convert_xlsx_to_csv() {
  echo ""
  echo -e $'Attempting to convert .xlsx file (\e[36mpost 2007\e[37m) \e[33m' "${file}" $'\e[37mto csv...'
  csv_file="$path$xls_filename_without_ext.csv"
 
  # if conversion succeeded
  if xlsx2csv "$file" > "$csv_file"; then
    xls2csv_exit_code=$?

    if [[ $xls2csv_exit_code -eq 0 ]]; then
      print_xlsx_conversion_success_message
      echo -e $'New file is: ' "$csv_file"
      echo ""

      # set global csv filename variable
      converted_csv_file="$csv_file"
      return 0
    else
      print_xlsx_conversion_failure_message
      exit 1
    fi
  else
    print_xlsx_conversion_failure_message
    exit 1
  fi
  return 0
}

# converts .xls file to .csv
convert_xls_to_csv() {
  echo ""
  echo -e $'Attempting to convert .xls file (\e[36mpre 2007\e[37m) \e[33m' "${file}" $'\e[37mto csv...'
  csv_file="$path/$xls_filename_without_ext.csv"
 
  if xls2csv "$file" > "$path/$xls_filename_without_ext.csv";  then
    xls2csv_exit_code=$?

    # if conversion succeeded
    if [[ $xls2csv_exit_code -eq 0 ]]; then
      print_xlsx_conversion_success_message
      echo -e $'New file is: ' "$path/$xls_filename_without_ext.csv"
      echo ""

      # set global csv filename variable
      converted_csv_file="$csv_file"
      return 0
    else
      print_xlsx_conversion_failure_message
      exit 1
    fi
  else
    print_xlsx_conversion_failure_message
    exit 1
  fi
  return 0
}

##################################################
#               Printer Functions                #
##################################################

print_error_title() {
  echo -e $'\e[31mERROR:\e[37m '
}

print_processing_csv_message() {
  echo ""
  echo -e $'\e[32mProcessing csv file\e[37m...' 
  echo ""
}

print_uninstalled_xls_failure_message() {
  echo ""
  print_error_title
  echo -e $'`\e[36mxls2csv\e[37m` is not installed, please install it by running: \e[33msudo apt-get install catdoc\e[37m.' 
  echo ""
}

print_uninstalled_xlsx_failure_message() {
  echo ""
  print_error_title
  echo -e $'`\e[36mxls2csv\e[37m` is not installed.'
  echo -e $'Please install it by running:'
      echo "-----------------------------------------------------------------"
  echo -e $'\e[36mUbuntu\e[37m: \e[33msudo apt-get install xlsx2csv\e[37m' 
  echo -e $'\e[36mArch (AUR)\e[37m:'
  echo -e $'In your `aur` folder, wherever it\'s located, do a \e[35mgit clone\e[37m on the following url:'
  echo -e $'\e[33mhttps://aur.archlinux.org/python-xlsx2csv.git\e[37m'
      echo "-----------------------------------------------------------------"
  echo ""
}

# prints out xlsx file conversion success message
print_xlsx_conversion_failure_message() {
  echo ""
  print_error_title
  echo -e $'\e[31mFailed to convert xlsx file!\e[37m'
  echo ""
}

print_unknown_xls_file_used_error_message() {
  echo ""
  print_error_title
  echo -e $'\e[31mUnsupported xls file used.\e[37m'
  echo ""
}

# prints out xlsx file conversion success message
print_xlsx_conversion_success_message() {
  echo ""
  echo -e $'\e[32mSuccessfully converted xlsx file!\e[37m'
  echo ""
}

##################################################
#               Main Script Logic                #
#                (Main Function)                 #
##################################################

# Check for path set by user,
# if not, set path to pwd
if [[ -z "$path" ]]; then
  set_path_to_pwd
fi

# Check if a file path is provided without the -f flag
if [[ -z "$file" && -n "$1" && ! "$1" =~ ^- ]]; then
  file="$1"
fi

if [[ ! -z "$file" ]]; then
  file_ext=$(get_xls_file_ext)
  set_xls_filename_without_ext

  if [[ "$file_ext" == "xls" ]]; then
    if check_for_xls2csv; then
      handle_xls_file
      return 0
    fi
  elif [[ "$file_ext" == "xlsx" ]]; then
    if check_for_xlsx2csv; then
      handle_xlsx_file
    fi
  else
    print_unknown_xls_file_used_error_message
  fi
elif [[ ! -z "$csv_file" ]]; then
  echo -e $'\e[33mWORK IN PROGRESS: THIS FLAG IS UNDER CONSTRUCTION...\e[37m'
  exit 1
else
  # error out if not a valid file
  echo -e $'\e[31m[-] ERROR: \e[39mYou must supply a file / csv file and an output filepath or file with path.'
  echo -e "  -> Use -h for the help menu"
  exit 1
fi

